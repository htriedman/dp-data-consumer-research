-- Parameters:
--     destination_directory -- Directory where to write transformation
--                              results
--
-- Usage:
--     spark3-sql --master yarn -f dp_dataset_2.hql       \
--         -d destination_directory=/wmf/tmp/dp_dataset_2 \
--         -d coalesce_partitions=1


SET spark.hadoop.hive.exec.compress.output=false;

WITH titles AS (
  SELECT
    CONCAT(CONCAT(w.language_code, '.'), w.database_group) as project,
    wipl.page_id,
    wipl.page_title,
    at.predicted_labels[1].label as predicted_topic
  FROM wmf.wikidata_item_page_link wipl
  JOIN canonical_data.wikis w
  ON wipl.wiki_db = w.database_code
  JOIN research.article_topics at
  ON wipl.wiki_db = at.wiki_db AND wipl.page_id = at.pid_from
  WHERE
    wipl.page_namespace = 0
    AND wipl.snapshot IN (
      SELECT MAX(DISTINCT snapshot)
      FROM wmf.wikidata_item_page_link
    )
    AND at.snapshot IN (
      SELECT MAX(DISTINCT snapshot)
      FROM research.article_topics
    )
  )

INSERT OVERWRITE DIRECTORY "${destination_directory}"
    -- Set 0 as volume column since we don't use it.
    USING csv
    OPTIONS ('compression' 'uncompressed', 'sep' '\t')

    SELECT /*+ COALESCE(${coalesce_partitions}) */
        cpp.country,
        cpp.country_code,
        cpp.project,
        titles.page_title,
        titles.predicted_topic,
        cpp.private_count AS views,
        cpp.year,
        cpp.month,
        cpp.day
    FROM differential_privacy.country_project_page cpp
    JOIN titles
    ON
        cpp.project = titles.project
        AND CAST(cpp.page_id AS BIGINT) = titles.page_id
    WHERE
        cpp.year = 2024
        AND cpp.month = 5
        AND cpp.day < 11
;
