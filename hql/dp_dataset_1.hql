-- Parameters:
--     destination_directory -- Directory where to write transformation
--                              results
--
-- Usage:
--     spark3-sql --master yarn -f dp_dataset_1.hql       \
--         -d destination_directory=/wmf/tmp/dp_dataset_1 \
--         -d coalesce_partions=1


SET spark.hadoop.hive.exec.compress.output=false;

WITH titles AS (
    SELECT 
        CONCAT(CONCAT(w.language_code, '.'), w.database_group) as project,
        wipl.page_id,
        wipl.page_title
    FROM wmf.wikidata_item_page_link wipl
    JOIN canonical_data.wikis w
    ON wipl.wiki_db = w.database_code
    WHERE
        wipl.page_namespace = 0
        AND wipl.snapshot IN (
            SELECT MAX(DISTINCT snapshot)
            FROM wmf.wikidata_item_page_link
        )
)

INSERT OVERWRITE DIRECTORY "${destination_directory}"
    -- Set 0 as volume column since we don't use it.
    USING csv
    OPTIONS ('compression' 'uncompressed', 'sep' '\t')

    SELECT /*+ COALESCE(${coalesce_partitions}) */
        cpp.country,
        cpp.country_code,
        cpp.project,
        titles.page_title,
        cpp.private_count AS views,
        cpp.year,
        cpp.month,
        cpp.day
    FROM differential_privacy.country_project_page cpp
    JOIN titles
    ON
        cpp.project = titles.project
        AND CAST(cpp.page_id AS BIGINT) = titles.page_id
    WHERE
        cpp.year = 2024
        AND cpp.month = 4
        AND cpp.day > 20
;

