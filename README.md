# dp-data-consumer-research

This is a repository containing code for a burgeoning collaborative research project about the relative usability of differentially-privatized pageview data (compared with rounded data and global raw pageview data).

This collaboration is between the following people:
- Jayshree Sarathy, Columbia University
- Priyanka Nanayakkara, Northwestern University
- Hal Triedman, WMF
- Rachel Cummings, Columbia University
- Gabe Kaptchuk, University of Maryland College Park
- Elissa Redmiles, Georgetown University

Please contact Hal at htriedman@wikimedia.org with any questions.

### License

The code in this repository is licensed under Apache 2.0. The data in this repository is licensed under CC-BY-SA.